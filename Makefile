lint:
	golangci-lint run
testDev:
	export ALLURE_RESULTS_PATH=~/golang_app/aff-test-framework
	rm -rf allure-results/*
	go clean -testcache
	go test ./... -v -env dev
testProd:
	go clean -testcache
	go test ./... -v -env prod
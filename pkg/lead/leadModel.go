package lead

type Response struct {
	Success bool `json:"success"`
	Data    Data `json:"data"`
}

type Data struct {
	LeadUUID string `json:"lead_uuid"`
	Status   string `json:"status,omitempty"`
}

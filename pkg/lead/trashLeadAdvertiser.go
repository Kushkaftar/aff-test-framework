package lead

import (
	"aff-test-framework/pkg/client"
	"encoding/json"
)

const trashLeadPath = "/api/lead/trash"

type trashLead struct {
	OrderID     string `json:"order_id"`
	TrashReason int    `json:"trash_reason"`
	IsFraud     bool   `json:"is_fraud"`
}

func (l *Lead) TrashLeadAdvertiser() (*client.Response, error) {
	cl := client.NewClient()
	url := l.Constructor.GetUrl(trashLeadPath)

	body := trashLead{
		OrderID:     l.LeadUUID,
		TrashReason: 1,
		IsFraud:     false,
	}

	jsonData, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	resp, err := cl.PostAuth(url, l.AdvApiKey, jsonData)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

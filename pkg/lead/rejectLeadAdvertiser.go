package lead

import (
	"aff-test-framework/pkg/client"
	"encoding/json"
)

const rejectLeadPath = "/api/lead/reject"

type rejectLead struct {
	OrderId      string `json:"order_id"`
	RejectReason int    `json:"reject_reason"`
	Status       bool   `json:"status"`
}

func (l *Lead) RejectLeadAdvertiser() (*client.Response, error) {
	cl := client.NewClient()
	url := l.Constructor.GetUrl(rejectLeadPath)

	body := rejectLead{
		OrderId:      l.LeadUUID,
		RejectReason: 1,
		Status:       false,
	}

	jsonData, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	resp, err := cl.PostAuth(url, l.AdvApiKey, jsonData)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

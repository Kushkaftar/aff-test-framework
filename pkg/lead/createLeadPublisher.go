package lead

import (
	"aff-test-framework/pkg/client"
	"encoding/json"
)

const leadPublisherPath = "/api/lead/create/publisher"

func (l *Lead) CreateLeadPublisher() (*client.Response, error) {
	cl := client.NewClient()
	url := l.Constructor.GetUrl(leadPublisherPath)

	jsonData, err := json.Marshal(&l.Body)
	if err != nil {
		return nil, err
	}

	resp, err := cl.PostAuth(url, l.PubApiKey, jsonData)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

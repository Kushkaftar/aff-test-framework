package lead

import (
	"aff-test-framework/pkg/client"
	"encoding/json"
)

const (
	orderKey        = "order_id"
	approveLeadPath = "/api/lead/approve"
)

func (l *Lead) ApproveLeadAdvertiser() (*client.Response, error) {
	cl := client.NewClient()
	url := l.Constructor.GetUrl(approveLeadPath)

	bodyMap := map[string]string{
		orderKey: l.LeadUUID,
	}

	jsonData, err := json.Marshal(bodyMap)
	if err != nil {
		return nil, err
	}

	resp, err := cl.PostAuth(url, l.AdvApiKey, jsonData)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

package lead

import (
	"aff-test-framework/pkg/client"
)

const leadAdvertiserPath = "/api/lead/create/advertiser"

func (l *Lead) CreateLeadAdvertiser() (*client.Response, error) {
	cl := client.NewClient()

	query := map[string]string{
		"api_key":            l.AdvApiKey,
		"redirect_uuid":      l.RedirectUUID,
		"goal_advertiser_id": l.AdvGoal,
		/*
			TODO: !!! продумать способ либо хранить либо сделать не возможным дублирование order_id
		*/
		"order_id": "test",
	}
	url := l.Constructor.SetQueryParams(leadAdvertiserPath, query)

	resp, err := cl.Get(url)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

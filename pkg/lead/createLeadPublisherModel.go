package lead

type LeadBodyPublisher struct {
	FlowUuid    string `json:"flow_uuid"`
	Ip          string `json:"ip"`
	CountryCode string `json:"country_code"`
	Phone       string `json:"phone"`
	Name        string `json:"name" faker:"name"`
	Comment     string `json:"comment" faker:"sentence"`
	Address     string `json:"address"`
	UtmCampaign string `json:"utm_campaign" faker:"word"`
	UtmContent  string `json:"utm_content" faker:"currency"`
	UtmMedium   string `json:"utm_medium" faker:"month_name"`
	UtmSource   string `json:"utm_source" faker:"day_of_month"`
	UtmTerm     string `json:"utm_term" faker:"day_of_week"`
	SubId1      string `json:"sub_id1" faker:"title_male"`
	SubId2      string `json:"sub_id2" faker:"title_male"`
	SubId3      string `json:"sub_id3" faker:"title_male"`
	UserAgent   string `json:"user_agent"`
	Referer     string `json:"referer" faker:"domain_name"`
	SysSubId1   string `json:"sys_sub_id1" faker:"title_female"`
	SysSubId2   string `json:"sys_sub_id2" faker:"title_female"`
	SysSubId3   string `json:"sys_sub_id3" faker:"title_female"`
	SysSubId4   string `json:"sys_sub_id4" faker:"title_female"`
	SysSubId5   string `json:"sys_sub_id5" faker:"title_female"`
}

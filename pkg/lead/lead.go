package lead

import (
	"aff-test-framework/pkg/config"
	"aff-test-framework/pkg/urlConstructor"
	"github.com/bxcodec/faker/v3"
	"log"
)

type Lead struct {
	LeadUUID     string
	RedirectUUID string
	PubApiKey    string
	AdvApiKey    string
	AdvGoal      string
	Body         LeadBodyPublisher
	Constructor  *urlConstructor.Scheme
}

func NewLead(c config.Config) *Lead {

	l, err := NewLeadBodyPublisher()
	if err != nil {
		log.Println("faker return error, label tags are not affixed")
	}

	l.FlowUuid = c.Publisher.Hash
	l.Ip = c.Main.IP
	l.CountryCode = c.Main.Geo
	l.Phone = "89100000000"

	nl := Lead{
		PubApiKey:   c.Publisher.ApiKey,
		AdvApiKey:   c.Advertiser.ApiKey,
		AdvGoal:     c.Advertiser.Goal,
		Constructor: urlConstructor.NewScheme(c.Main.Scheme, c.Main.HostApi),
	}

	nl.Body = *l

	return &nl
}

func NewLeadBodyPublisher() (*LeadBodyPublisher, error) {
	nl := LeadBodyPublisher{}

	err := faker.FakeData(&nl)
	if err != nil {
		return nil, err
	}

	return &nl, nil
}

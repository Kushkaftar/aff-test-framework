package lead

import "encoding/json"

func ResponseDecode(body []uint8) (*Response, error) {
	data := Response{}

	err := json.Unmarshal(body, &data)
	if err != nil {
		return nil, err
	}

	return &data, nil
}

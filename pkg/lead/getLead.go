package lead

import "aff-test-framework/pkg/client"

const (
	leadUUIDKey = "lead_uuid"
	getLead     = "/api/lead/get-status"
)

func (l *Lead) GetLead() (*client.Response, error) {
	cl := client.NewClient()

	query := map[string]string{leadUUIDKey: l.LeadUUID}
	url := l.Constructor.SetQueryParams(getLead, query)

	resp, err := cl.GetAuth(url, l.PubApiKey)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

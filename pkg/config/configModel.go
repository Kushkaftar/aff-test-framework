package config

type Config struct {
	Main       Main       `yaml:"main"`
	Publisher  Publisher  `yaml:"publisher"`
	Advertiser Advertiser `yaml:"advertiser"`
}

type Publisher struct {
	ApiKey string `yaml:"apiKey"`
	Hash   string `yaml:"hash"`
}

type Advertiser struct {
	ApiKey string `yaml:"apiKey"`
	Goal   string `yaml:"goal"`
}

type Main struct {
	Scheme       string `yaml:"scheme"`
	HostApi      string `yaml:"hostApi"`
	HostRedirect string `yaml:"hostRedirect"`
	IP           string `yaml:"ip"`
	Geo          string `yaml:"geo"`
}

package client

import (
	"io/ioutil"
	"net/http"
)

func (c Client) Get(url string) (*Response, error) {

	req, err := http.NewRequest(
		http.MethodGet, url, nil,
	)
	if err != nil {
		return nil, err
	}
	//TODO: костыль, передавать хедер параметром
	req.Header.Set(XForwardedForHeader, "91.246.127.113")

	resp, err := c.HttpClient.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	response := Response{}
	response.Header = resp.Header
	response.Body = body
	response.StatusCode = resp.StatusCode

	return &response, nil
}

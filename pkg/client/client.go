package client

import "net/http"

type Client struct {
	HttpClient *http.Client
}

const (
	apiKeyHeader        = "ApiKey"
	XForwardedForHeader = "X-Forwarded-For"
)

func NewClient() *Client {
	client := Client{}
	client.HttpClient = &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}}
	return &client
}

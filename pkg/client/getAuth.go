package client

import (
	"io/ioutil"
	"net/http"
)

func (c Client) GetAuth(url, apiKey string) (*Response, error) {
	req, err := http.NewRequest(
		http.MethodGet, url, nil,
	)
	if err != nil {
		return nil, err
	}
	req.Header.Set(apiKeyHeader, apiKey)

	resp, err := c.HttpClient.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	response := Response{}
	response.Header = resp.Header
	response.Body = body
	response.StatusCode = resp.StatusCode

	return &response, nil
}

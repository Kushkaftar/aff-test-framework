package client

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

func (c Client) PostAuth(url, apiKey string, body []uint8) (*Response, error) {
	req, err := http.NewRequest(
		http.MethodPost, url, bytes.NewBuffer(body),
	)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set(apiKeyHeader, apiKey)

	resp, err := c.HttpClient.Do(req)
	if err != nil {
		return nil, err
	}

	bodyResp, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	response := Response{}
	response.Header = resp.Header
	response.Body = bodyResp
	response.StatusCode = resp.StatusCode

	return &response, nil
}

package urlConstructor

import "net/url"

func (s Scheme) GetQueryParameter(path, parameter string) (string, error) {
	u, err := url.Parse(path)
	if err != nil {
		return "", err
	}

	q := u.Query()
	p := q.Get(parameter)

	return p, nil
}

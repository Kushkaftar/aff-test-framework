package urlConstructor

type Scheme struct {
	Scheme string
	Host   string
}

func NewScheme(scheme, host string) *Scheme {
	return &Scheme{
		Scheme: scheme,
		Host:   host,
	}
}

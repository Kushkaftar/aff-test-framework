package urlConstructor

import "net/url"

func (s Scheme) GetUrl(path string) string {
	u := url.URL{
		Scheme: s.Scheme,
		Host:   s.Host,
		Path:   path,
	}
	return u.String()
}

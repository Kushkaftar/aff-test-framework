package urlConstructor

import (
	"net/url"
	"strings"
)

func (s Scheme) SetQueryParams(path string, query map[string]string) string {
	u := url.URL{
		Scheme: s.Scheme,
		Host:   s.Host,
		Path:   path,
	}

	q := u.Query()

	if len(query) != 0 {

		for key, value := range query {
			if strings.Trim(key, " ") != "" {
				q.Add(key, value)
			}

		}
	}

	u.RawQuery = q.Encode()

	return u.String()
}

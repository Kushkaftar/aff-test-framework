package redirect

import (
	"aff-test-framework/pkg/client"
	"encoding/json"
)

const remotePath = "/r/remote"

func (r Redirect) Remote() (*client.Response, error) {
	cl := client.NewClient()
	//todo: refactor
	flow := remote{
		FlowUUID:    r.Hash,
		SubID1:      r.Labels.SubId1,
		SubID2:      r.Labels.SubId2,
		SubID3:      r.Labels.SubId3,
		UtmContent:  r.Labels.UtmContent,
		UtmTerm:     r.Labels.UtmTerm,
		UtmSource:   r.Labels.UtmSource,
		UtmCampaign: r.Labels.UtmCampaign,
		UtmMedium:   r.Labels.UtmMedium,
	}
	jsonData, err := json.Marshal(&flow)
	if err != nil {
		return nil, err
	}

	url := r.Constructor.GetUrl(remotePath)

	resp, err := cl.Post(url, jsonData)
	if err != nil {
		return nil, err
	}
	return resp, err
}

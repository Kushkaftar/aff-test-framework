package redirect

type RedirectLabels struct {
	SubID1      string `json:"sub_id1"`
	SubID2      string `json:"sub_id2"`
	SubID3      string `json:"sub_id3"`
	UtmCampaign string `json:"utm_campaign"`
	UtmContent  string `json:"utm_content"`
	UtmMedium   string `json:"utm_medium"`
	UtmSource   string `json:"utm_source"`
	UtmTerm     string `json:"utm_term"`
}

type updateVisitToLand struct {
	RID         string `json:"r_id"`
	LandingUUID string `json:"landing_uuid"`
}

type remote struct {
	FlowUUID    string `json:"flow_uuid"`
	SubID1      string `json:"sub_id1"`
	SubID2      string `json:"sub_id2"`
	SubID3      string `json:"sub_id3"`
	UtmCampaign string `json:"utm_campaign"`
	UtmContent  string `json:"utm_content"`
	UtmMedium   string `json:"utm_medium"`
	UtmSource   string `json:"utm_source"`
	UtmTerm     string `json:"utm_term"`
}

type Response struct {
	Success bool `json:"success"`
	Data    Data `json:"data"`
}

type Data struct {
	URL          string `json:"url"`
	LeadUUID     string `json:"lead_uuid"`
	Status       string `json:"status"`
	RedirectUUID string `json:"r_id"`
}

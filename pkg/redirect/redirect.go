package redirect

import (
	"aff-test-framework/pkg/urlConstructor"
	"github.com/bxcodec/faker/v3"
	"log"
)

type Redirect struct {
	Hash        string
	LandingUUID string
	LeadUIID    string
	LeadParams  RedirectLeadParams
	Labels      Labels
	Constructor *urlConstructor.Scheme
}
type RedirectLeadParams struct {
	RID         string `json:"r_id"`
	CountryCode string `json:"country_code"`
	Phone       string `json:"phone"`
	Name        string `json:"name"`
}

type Labels struct {
	UtmCampaign string `json:"utm_campaign" faker:"word"`
	UtmContent  string `json:"utm_content" faker:"currency"`
	UtmMedium   string `json:"utm_medium" faker:"month_name"`
	UtmSource   string `json:"utm_source" faker:"day_of_month"`
	UtmTerm     string `json:"utm_term" faker:"day_of_week"`
	SubId1      string `json:"sub_id1" faker:"title_male"`
	SubId2      string `json:"sub_id2" faker:"title_male"`
	SubId3      string `json:"sub_id3" faker:"title_male"`
}

func NewRedirect(hash, scheme, host string) *Redirect {
	labels, err := newLabels()
	if err != nil {
		log.Println("faker return error, label tags are not affixed")
	}

	rlp := RedirectLeadParams{
		CountryCode: "RU",
		Phone:       "89100000000",
		Name:        "Name",
	}

	return &Redirect{
		Hash:        hash,
		Labels:      *labels,
		LeadParams:  rlp,
		Constructor: urlConstructor.NewScheme(scheme, host),
	}
}

func newLabels() (*Labels, error) {
	l := Labels{}

	err := faker.FakeData(&l)
	if err != nil {
		return nil, err
	}

	return &l, nil
}

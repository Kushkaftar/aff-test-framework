package redirect

import (
	"aff-test-framework/pkg/client"
	"fmt"
	"log"
)

const redirectPath = "/r/"

func (r Redirect) GetRID() (*client.Response, error) {
	cl := client.NewClient()

	pathHash := fmt.Sprintf("%s%s", redirectPath, r.Hash)
	query := map[string]string{
		"utm_campaign": r.Labels.UtmCampaign,
		"utm_content":  r.Labels.UtmContent,
		"utm_medium":   r.Labels.UtmMedium,
		"utm_source":   r.Labels.UtmSource,
		"utm_term":     r.Labels.UtmTerm,
		"sub_id1":      r.Labels.SubId1,
		"sub_id2":      r.Labels.SubId2,
		"sub_id3":      r.Labels.SubId3,
	}
	url := r.Constructor.SetQueryParams(pathHash, query)
	log.Printf("---- url - %s", url)
	resp, err := cl.Get(url)
	if err != nil {
		return nil, err
	}
	return resp, err
}

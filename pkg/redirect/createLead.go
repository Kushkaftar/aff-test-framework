package redirect

import (
	"aff-test-framework/pkg/client"
	"encoding/json"
)

const createLeadPath = "/l/create"

func (r Redirect) CreateLead() (*client.Response, error) {
	cl := client.NewClient()

	jsonData, err := json.Marshal(&r.LeadParams)
	if err != nil {
		return nil, err
	}

	url := r.Constructor.GetUrl(createLeadPath)

	resp, err := cl.Post(url, jsonData)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

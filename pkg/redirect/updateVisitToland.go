package redirect

import (
	"aff-test-framework/pkg/client"
	"encoding/json"
)

const updatePath = "/r/update"

func (r Redirect) UpdateVisitToLand() (*client.Response, error) {
	cl := client.NewClient()

	body := updateVisitToLand{
		RID:         r.LeadParams.RID,
		LandingUUID: r.LandingUUID,
	}

	jsonData, err := json.Marshal(&body)
	if err != nil {
		return nil, err
	}

	url := r.Constructor.GetUrl(updatePath)

	resp, err := cl.Post(url, jsonData)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

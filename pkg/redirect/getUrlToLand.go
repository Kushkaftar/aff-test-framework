package redirect

import "aff-test-framework/pkg/client"

const (
	urlToLandPath = "/r/url"
	rIDKey        = "r_id"
)

func (r Redirect) GetUrlToLand() (*client.Response, error) {
	cl := client.NewClient()

	query := map[string]string{rIDKey: r.LeadParams.RID}
	url := r.Constructor.SetQueryParams(urlToLandPath, query)

	resp, err := cl.Get(url)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

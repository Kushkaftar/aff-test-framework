package tests

import (
	"aff-test-framework/pkg/config"
	"flag"
	"log"
	"os"
	"testing"
)

type testConfig struct {
	c config.Config
}

const pathToConfig = "../configs"

var c = testConfig{}

func TestMain(m *testing.M) {
	var fileName string

	flag.StringVar(&fileName, "env", "", "desc")
	flag.Parse()

	newConfig(fileName, pathToConfig)
	//log.Printf("config - %+v", c)
	os.Exit(m.Run())
}

func newConfig(filename, path string) {
	cfg, err := config.NewConfig(filename, path)
	if err != nil {
		log.Fatalln("crush init config")
	}

	c.c = *cfg
}

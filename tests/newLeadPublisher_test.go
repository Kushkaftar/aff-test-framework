package tests

import (
	"aff-test-framework/suites"
	"testing"
)

func TestNewLeadPublisher(t *testing.T) {
	suites.TestNewLeadPublisher(t, c.c)
}

func TestNewLeadPublisherNegative(t *testing.T) {
	suites.TestNewLeadPublisherNegative(t, c.c)
}

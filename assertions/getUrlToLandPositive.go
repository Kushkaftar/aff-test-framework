package assertions

import (
	"aff-test-framework/pkg/client"
	"aff-test-framework/pkg/redirect"
	"fmt"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"net/http"
)

const landingUuidParam = "landing_uuid"

func (a Assertion) GetUrlToLandPositive(r *redirect.Redirect) {

	allure.Step(
		allure.Description(fmt.Sprintf("get url to land, r_id - %s", r.LeadParams.RID)),
		allure.Action(func() {
			var resp *client.Response
			var body *redirect.Response

			allure.Step(
				allure.Description("get json link to promo"),
				allure.Action(func() {
					allure.Parameter("resp", resp)

					response, err := r.GetUrlToLand()
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(response.StatusCode).Should(gomega.Equal(http.StatusOK))

					resp = response
				}),
			)

			allure.Step(
				allure.Description("unmarshall response body to struct"),
				allure.Action(func() {
					allure.Parameter("body", body)

					jsonStruct, err := redirect.ResponseDecode(resp.Body)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					body = jsonStruct
				}),
			)

			allure.Step(
				allure.Description("expect success and get landing uuid"),
				allure.Action(func() {
					//allure.Parameter("body", body)

					a.G.Expect(body.Success).To(gomega.BeTrue())

					landingUuid, err := r.Constructor.GetQueryParameter(body.Data.URL, landingUuidParam)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(landingUuid).ShouldNot(gomega.BeEmpty())
					r.LandingUUID = landingUuid
				}),
			)

		}),
	)

}

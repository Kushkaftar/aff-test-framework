package assertions

import (
	"aff-test-framework/pkg/lead"
	"encoding/json"
	"fmt"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"net/http"
)

func (a Assertion) CreateApiLeadPublisherNegative(l *lead.Lead) (text, field string) {
	var responseBody []uint8
	body := createApiLeadPublisherNegative{}

	allure.Step(
		allure.Description(fmt.Sprintf("send lead, body - %+v", l.Body)),
		allure.Action(func() {
			resp, err := l.CreateLeadPublisher()
			a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

			a.G.Expect(resp.StatusCode).Should(gomega.Equal(http.StatusOK))
			responseBody = resp.Body
		}),
	)

	allure.Step(
		allure.Description(fmt.Sprintf("json unmarshal, response body - %s", string(responseBody))),
		allure.Action(func() {

			err := json.Unmarshal(responseBody, &body)
			a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

			a.G.Expect(body.Success).NotTo(gomega.BeTrue())
		}),
	)

	// TODO: переписать на возврат мапы
	return body.Errors[0].Text, body.Errors[0].Field
}

type createApiLeadPublisherNegative struct {
	Success bool     `json:"success"`
	Errors  []erorrs `json:"errors"`
}

type erorrs struct {
	Text  string `json:"text"`
	Field string `json:"field"`
}

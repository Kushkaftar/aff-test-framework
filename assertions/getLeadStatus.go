package assertions

import (
	"aff-test-framework/pkg/client"
	"aff-test-framework/pkg/lead"
	"encoding/json"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"net/http"
)

func (a Assertion) GetLeadStatusPositive(l *lead.Lead, status string) {
	type data struct {
		LeadUUID    string `json:"lead_uuid"`
		Status      string `json:"status"`
		SubID1      string `json:"subid1"`
		SubID2      string `json:"subid2"`
		SubID3      string `json:"subid3"`
		UtmCampaign string `json:"utm_campaign"`
		UtmContent  string `json:"utm_content"`
		UtmMedium   string `json:"utm_medium"`
		UtmTerm     string `json:"utm_term"`
		UtmSource   string `json:"utm_source"`
	}

	type leadResp struct {
		Success bool `json:"success"`
		Data    data `json:"data"`
	}

	allure.Step(
		allure.Description("get lead info and check labels"),
		allure.Action(func() {
			var (
				resp *client.Response
				body leadResp
			)

			allure.Step(
				allure.Description("get lead info"),
				allure.Action(func() {
					allure.Parameter("resp", resp)

					response, err := l.GetLead()
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(response.StatusCode).Should(gomega.Equal(http.StatusOK))

					resp = response
				}),
			)

			allure.Step(
				allure.Description("unmarshall body"),
				allure.Action(func() {

					err := json.Unmarshal(resp.Body, &body)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())
				}),
			)
			allure.Step(
				allure.Description("expected success and labels"),
				allure.Action(func() {

					a.G.Expect(body.Success).To(gomega.BeTrue())
					a.G.Expect(body.Data.Status).Should(gomega.Equal(status))
					a.G.Expect(body.Data.LeadUUID).Should(gomega.Equal(l.LeadUUID))

					// сравнение переданных меток
					//todo: refactor
					a.G.Expect(body.Data.SubID1).Should(gomega.Equal(l.Body.SubId1))
					a.G.Expect(body.Data.SubID2).Should(gomega.Equal(l.Body.SubId2))
					a.G.Expect(body.Data.SubID3).Should(gomega.Equal(l.Body.SubId3))

					a.G.Expect(body.Data.UtmCampaign).Should(gomega.Equal(l.Body.UtmCampaign))
					a.G.Expect(body.Data.UtmMedium).Should(gomega.Equal(l.Body.UtmMedium))
					a.G.Expect(body.Data.UtmSource).Should(gomega.Equal(l.Body.UtmSource))
					a.G.Expect(body.Data.UtmContent).Should(gomega.Equal(l.Body.UtmContent))
					a.G.Expect(body.Data.UtmTerm).Should(gomega.Equal(l.Body.UtmTerm))
				}),
			)
		}),
	)
}

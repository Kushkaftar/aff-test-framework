package assertions

import (
	"aff-test-framework/pkg/lead"
	"github.com/onsi/gomega"
	"net/http"
)

func (a Assertion) CreateLeadAdvertiserPositive(l *lead.Lead) {
	resp, err := l.CreateLeadAdvertiser()
	a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

	a.G.Expect(resp.StatusCode).Should(gomega.Equal(http.StatusOK))

	body, err := lead.ResponseDecode(resp.Body)
	a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

	a.G.Expect(body.Success).To(gomega.BeTrue())
	a.G.Expect(body.Data.LeadUUID).ShouldNot(gomega.BeEmpty())

	l.LeadUUID = body.Data.LeadUUID

}

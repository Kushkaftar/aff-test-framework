package assertions

import (
	"aff-test-framework/pkg/client"
	"aff-test-framework/pkg/redirect"
	"encoding/json"
	"fmt"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"net/http"
)

func (a Assertion) UpdateVisitToLandPositive(r *redirect.Redirect) {

	allure.Step(
		allure.Description(fmt.Sprintf("update visit ti land, r_id - %s, landing_uuid - %s", r.LeadParams.RID, r.LandingUUID)),
		allure.Action(func() {
			var resp *client.Response

			allure.Step(
				allure.Description("send request"),
				allure.Action(func() {
					allure.Parameter("resp", resp)

					response, err := r.UpdateVisitToLand()
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(response.StatusCode).Should(gomega.Equal(http.StatusOK))

					resp = response
				}),
			)

			allure.Step(
				allure.Description("unmarshall json, and expected success is true"),
				allure.Action(func() {
					body := struct {
						Success bool `json:"success"`
					}{}
					err := json.Unmarshal(resp.Body, &body)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(body.Success).To(gomega.BeTrue())
				}),
			)

		}),
	)

}

package assertions

import (
	"aff-test-framework/pkg/client"
	"aff-test-framework/pkg/redirect"
	"fmt"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"net/http"
)

const (
	rIdParam = "r_id"
	//countryCodeParam = "country_code"
)

func (a Assertion) GetRIDPositive(r *redirect.Redirect) {

	allure.Step(
		allure.Description(fmt.Sprintf("send request, hash - %s", r.Hash)),
		allure.Action(func() {
			var resp *client.Response
			var headerLocation string

			allure.Step(
				allure.Description("transition to flow link"),
				allure.Action(func() {
					allure.Parameter("resp", resp)

					response, err := r.GetRID()
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(response.StatusCode).Should(gomega.Equal(http.StatusFound))
					resp = response
				}),
			)

			allure.Step(

				allure.Description("get headers location"),
				allure.Action(func() {
					allure.Parameter("headerLocation", headerLocation)

					headerLocation = resp.Header["Location"][0]
					a.G.Expect(headerLocation).ShouldNot(gomega.BeEmpty())
				}),
			)
			allure.Step(
				allure.Description("get r_id"),
				allure.Action(func() {

					redirectID, err := r.Constructor.GetQueryParameter(headerLocation, rIdParam)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(redirectID).ShouldNot(gomega.BeEmpty())
					r.LeadParams.RID = redirectID
				}),
			)

		}),
	)
}

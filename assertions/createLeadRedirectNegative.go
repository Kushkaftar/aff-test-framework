package assertions

import (
	"aff-test-framework/pkg/client"
	"aff-test-framework/pkg/redirect"
	"encoding/json"
	"fmt"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"log"
	"net/http"
)

func (a Assertion) CreateLeadRedirectNegative(r *redirect.Redirect) (text, field string) {
	var body createApiLeadPublisherNegative

	allure.Step(
		allure.Description("create lead"),
		allure.Action(func() {
			var resp *client.Response

			str := fmt.Sprintf("body - %+v", r)
			err := allure.AddAttachment("body", allure.TextPlain, []byte(str))
			a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

			allure.Step(
				allure.Description("send lead"),
				allure.Action(func() {
					allure.Parameter("resp", resp)

					log.Printf("lead body - %+v", r.LeadParams)
					response, err := r.CreateLead()
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(response.StatusCode).Should(gomega.Equal(http.StatusOK))

					resp = response
				}),
			)

			allure.Step(
				allure.Description("unmarshal json response, and expected success"),
				allure.Action(func() {

					err := json.Unmarshal(resp.Body, &body)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(body.Success).NotTo(gomega.BeTrue())
				}),
			)
		}),
	)

	//todo: refactor !!!
	log.Printf("body - %+v", body)
	return body.Errors[0].Text, body.Errors[0].Field
}

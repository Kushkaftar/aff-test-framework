package assertions

import (
	"aff-test-framework/pkg/redirect"
	"fmt"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
)

func (a Assertion) GetRIDNegative(r *redirect.Redirect) {
	allure.Step(
		allure.Description(fmt.Sprintf("Hash: \"%s\"", r.Hash)),
		allure.Action(func() {
			resp, err := r.GetRID()
			a.G.Expect(err).ShouldNot(gomega.HaveOccurred())
			//log.Printf("resp body - %s", string(resp.Body))
			a.G.Expect(resp.StatusCode).Should(gomega.Equal(200))
			//todo: expected html and give title "Access denied"
		}),
	)

}

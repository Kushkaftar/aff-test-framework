package assertions

import (
	"aff-test-framework/pkg/client"
	"aff-test-framework/pkg/lead"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"net/http"
)

func (a Assertion) CreateApiLeadPublisherPositive(l *lead.Lead) {

	allure.Step(
		allure.Description("create lead and expected status, success"),
		allure.Action(func() {
			var (
				resp *client.Response
				body *lead.Response
			)

			allure.Step(
				allure.Description("create lead and expected status code 200"),
				allure.Action(func() {
					allure.Parameter("resp", resp)

					response, err := l.CreateLeadPublisher()
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(response.StatusCode).Should(gomega.Equal(http.StatusOK))

					err = allure.AddAttachment("response body", allure.TextPlain, response.Body)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					resp = response
				}),
			)

			allure.Step(
				allure.Description("unmarshall response body"),
				allure.Action(func() {
					allure.Parameter("body", body)

					jsonData, err := lead.ResponseDecode(resp.Body)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					body = jsonData
				}),
			)

			allure.Step(
				allure.Description("expected success and lead uuid not empty"),
				allure.Action(func() {

					a.G.Expect(body.Success).To(gomega.BeTrue())
					a.G.Expect(body.Data.LeadUUID).ShouldNot(gomega.BeEmpty())

					l.LeadUUID = body.Data.LeadUUID
				}),
			)
		}),
	)

}

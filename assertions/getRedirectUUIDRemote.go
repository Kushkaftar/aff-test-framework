package assertions

import (
	"aff-test-framework/pkg/redirect"
	"github.com/onsi/gomega"
	"net/http"
)

func (a Assertion) GetRedirectUUIDRemotePositive(r *redirect.Redirect) {
	resp, err := r.Remote()
	a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

	a.G.Expect(resp.StatusCode).Should(gomega.Equal(http.StatusOK))

	body, err := redirect.ResponseDecode(resp.Body)
	a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

	a.G.Expect(body.Success).To(gomega.BeTrue())
	a.G.Expect(body.Data.RedirectUUID).ShouldNot(gomega.BeEmpty())

	r.LeadParams.RID = body.Data.RedirectUUID
}

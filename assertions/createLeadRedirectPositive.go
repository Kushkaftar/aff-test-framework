package assertions

import (
	"aff-test-framework/pkg/client"
	"aff-test-framework/pkg/redirect"
	"fmt"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"net/http"
)

func (a Assertion) CreateLeadRedirectPositive(r *redirect.Redirect) {

	allure.Step(
		allure.Description("create lead"),
		allure.Action(func() {
			var resp *client.Response

			str := fmt.Sprintf("body - %+v", r)
			err := allure.AddAttachment("body", allure.TextPlain, []byte(str))
			a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

			allure.Step(
				allure.Description("send lead"),
				allure.Action(func() {
					allure.Parameter("resp", resp)

					response, err := r.CreateLead()
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(response.StatusCode).Should(gomega.Equal(http.StatusOK))

					resp = response
				}),
			)

			allure.Step(
				allure.Description("unmarshal json response? and expected success"),
				allure.Action(func() {
					body, err := redirect.ResponseDecode(resp.Body)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(body.Success).To(gomega.BeTrue())

					a.G.Expect(body.Data.LeadUUID).ShouldNot(gomega.BeEmpty())
					r.LeadUIID = body.Data.LeadUUID
				}),
			)
		}),
	)

}

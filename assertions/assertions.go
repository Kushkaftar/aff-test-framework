package assertions

import (
	"errors"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"runtime/debug"
	"testing"
)

type Assertion struct {
	G *gomega.WithT
}

func NewAssertion(t *testing.T) *Assertion {
	fail := func(message string, callerSkip ...int) {
		// добавление вызова allure.fail для выгрузки в отчет ошибки
		allure.Fail(errors.New(message))
		t.Logf("\\n%s %s", message, debug.Stack())
	}

	g := gomega.NewGomegaWithT(t)
	g.ConfigureWithFailHandler(fail)
	return &Assertion{
		G: g,
	}
}

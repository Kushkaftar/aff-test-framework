package suites

import (
	"aff-test-framework/assertions"
	"aff-test-framework/pkg/config"
	"aff-test-framework/pkg/lead"
	"aff-test-framework/pkg/redirect"
	"github.com/dailymotion/allure-go"
	"testing"
)

const statusNew = "new"

func TestNewLeadAdvertiser(t *testing.T, c config.Config) {

	l := lead.NewLead(c)
	r := redirect.NewRedirect(c.Publisher.Hash, c.Main.Scheme, c.Main.HostRedirect)

	t.Run("test crate lead advertiser", func(t *testing.T) {
		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)
			allure.Step(
				allure.Description("create r_id from remote"),
				allure.Action(func() {
					a.GetRedirectUUIDRemotePositive(r)
				}),
			)
			allure.Step(
				allure.Description("create lead advertiser"),
				allure.Action(func() {
					l.RedirectUUID = r.LeadParams.RID
					a.CreateLeadAdvertiserPositive(l)
				}),
			)
			allure.Step(
				allure.Description("get lead status and expected labels"),
				allure.Action(func() {
					//todo: refactor
					l.Body.SubId1 = r.Labels.SubId1
					l.Body.SubId2 = r.Labels.SubId2
					l.Body.SubId3 = r.Labels.SubId3
					l.Body.UtmMedium = r.Labels.UtmMedium
					l.Body.UtmCampaign = r.Labels.UtmCampaign
					l.Body.UtmSource = r.Labels.UtmSource
					l.Body.UtmTerm = r.Labels.UtmTerm
					l.Body.UtmContent = r.Labels.UtmContent
					a.GetLeadStatusPositive(l, statusNew)
				}),
			)

		}))
	})
}

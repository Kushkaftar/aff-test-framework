package suites

import (
	"aff-test-framework/assertions"
	"aff-test-framework/pkg/config"
	"aff-test-framework/pkg/lead"
	"encoding/json"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"net/http"
	"testing"
)

type respSetStatusAdvert struct {
	Success bool `json:"success"`
	Data    struct {
		OrderID  string `json:"orderID"`
		LeadUUID string `json:"lead_uuid"`
	} `json:"data"`
}

const (
	statusApproved = "approved"
	statusReject   = "reject"
	statusTrash    = "trash"
)

func SetStatusesAdvertiser(t *testing.T, c config.Config) {

	t.Run("set status approve", func(t *testing.T) {
		l := lead.NewLead(c)

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			allure.Step(
				allure.Description("create api lead publisher"),
				allure.Action(func() {

					a.CreateApiLeadPublisherPositive(l)
				}),
			)

			allure.Step(
				allure.Description("set status approved"),
				allure.Action(func() {
					resp, err := l.ApproveLeadAdvertiser()
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(resp.StatusCode).Should(gomega.Equal(http.StatusOK))

					body := respSetStatusAdvert{}
					err = json.Unmarshal(resp.Body, &body)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(body.Success).To(gomega.BeTrue())
				}),
			)

			allure.Step(
				allure.Description("get lead status and expected labels"),
				allure.Action(func() {
					a.GetLeadStatusPositive(l, statusApproved)
				}),
			)
		}))
	})

	t.Run("set status rejected", func(t *testing.T) {
		l := lead.NewLead(c)

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			allure.Step(
				allure.Description("create api lead publisher"),
				allure.Action(func() {
					a.CreateApiLeadPublisherPositive(l)
				}),
			)

			allure.Step(
				allure.Description("set status reject"),
				allure.Action(func() {
					resp, err := l.RejectLeadAdvertiser()
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(resp.StatusCode).Should(gomega.Equal(http.StatusOK))

					body := respSetStatusAdvert{}
					err = json.Unmarshal(resp.Body, &body)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(body.Success).To(gomega.BeTrue())
				}),
			)

			allure.Step(
				allure.Description("get lead status and expected labels"),
				allure.Action(func() {
					a.GetLeadStatusPositive(l, statusReject)
				}),
			)
		}))
	})

	t.Run("set status trash", func(t *testing.T) {
		l := lead.NewLead(c)

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			allure.Step(
				allure.Description("create api lead publisher"),
				allure.Action(func() {
					a.CreateApiLeadPublisherPositive(l)
				}),
			)

			allure.Step(
				allure.Description("set status trash"),
				allure.Action(func() {
					resp, err := l.TrashLeadAdvertiser()
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(resp.StatusCode).Should(gomega.Equal(http.StatusOK))

					body := respSetStatusAdvert{}
					err = json.Unmarshal(resp.Body, &body)
					a.G.Expect(err).ShouldNot(gomega.HaveOccurred())

					a.G.Expect(body.Success).To(gomega.BeTrue())
				}),
			)

			allure.Step(
				allure.Description("get lead status and expected labels"),
				allure.Action(func() {
					a.GetLeadStatusPositive(l, statusTrash)
				}),
			)
		}))
	})
}

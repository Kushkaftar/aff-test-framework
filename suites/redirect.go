package suites

import (
	"aff-test-framework/assertions"
	"aff-test-framework/pkg/config"
	"aff-test-framework/pkg/lead"
	"aff-test-framework/pkg/redirect"
	"github.com/dailymotion/allure-go"
	"testing"
)

func TestRedirectSuite(t *testing.T, c config.Config) {

	r := redirect.NewRedirect(c.Publisher.Hash, c.Main.Scheme, c.Main.HostRedirect)
	l := lead.NewLead(c)

	t.Run("get r_id", func(t *testing.T) {
		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			allure.Step(
				allure.Description("get r_id"),
				allure.Action(func() {
					a.GetRIDPositive(r)
				}),
			)

			allure.Step(
				allure.Description("get url to land"),
				allure.Action(func() {
					a.GetUrlToLandPositive(r)
				}),
			)

			allure.Step(
				allure.Description("update visit to land"),
				allure.Action(func() {
					a.UpdateVisitToLandPositive(r)
				}),
			)

			allure.Step(
				allure.Description("create lead"),
				allure.Action(func() {
					a.CreateLeadRedirectPositive(r)
				}),
			)

			allure.Step(
				allure.Description("get lead status and expected labels"),
				allure.Action(func() {
					l.LeadUUID = r.LeadUIID
					//todo: refactor
					l.Body.SubId1 = r.Labels.SubId1
					l.Body.SubId2 = r.Labels.SubId2
					l.Body.SubId3 = r.Labels.SubId3
					l.Body.UtmMedium = r.Labels.UtmMedium
					l.Body.UtmCampaign = r.Labels.UtmCampaign
					l.Body.UtmSource = r.Labels.UtmSource
					l.Body.UtmTerm = r.Labels.UtmTerm
					l.Body.UtmContent = r.Labels.UtmContent
					a.GetLeadStatusPositive(l, statusNew)
				}),
			)

		}))

	})
}

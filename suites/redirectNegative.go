package suites

import (
	"aff-test-framework/assertions"
	"aff-test-framework/pkg/config"
	"aff-test-framework/pkg/redirect"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"log"
	"strings"
	"testing"
)

func TestRedirectNegativeSuite(t *testing.T, c config.Config) {
	const (
		brokenHashText  = "Lead creation is not allowed"
		wrongHashText   = "Значение не соответствует формату UUID."
		wrongIpText     = "Redirect create exception: Not defined country by ip 10.10.10.10"
		notValidGeoText = "This value is not a valid country."
		wrongGeoText    = "Goal not found"
		wrongPhoneText  = "Некорректно введен номер телефона"
		emptyRIDText    = "Переданный uuid невалиден"
		emptyRuText     = "Значение не должно быть пустым."
		emptyEnText     = "This value should not be blank."
		fieldFlow       = "flow_uuid"
		fieldGeo        = "country_code"
		fieldIP         = "ip"
		fieldPhone      = "phone"
		fieldRID        = "r_id"
	)

	r := redirect.NewRedirect(c.Publisher.Hash, c.Main.Scheme, c.Main.HostRedirect)
	a := assertions.NewAssertion(t)

	allure.BeforeTest(t,
		allure.Description("get redirect uuid"),
		allure.Action(func() {
			a.GetRedirectUUIDRemotePositive(r)
		}))

	allure.Test(t,
		allure.Description("not transferred phone"),
		allure.Action(func() {
			notPhone := *r
			notPhone.LeadParams.Phone = ""
			text, field := a.CreateLeadRedirectNegative(&notPhone)

			a.G.Expect(text).Should(gomega.Equal(emptyRuText))
			a.G.Expect(field).Should(gomega.Equal(fieldPhone))
		}),
	)
	allure.Test(t,
		allure.Description("wrong phone"),
		allure.Action(func() {
			notPhone := *r
			notPhone.LeadParams.Phone = "+5734200000"
			text, _ := a.CreateLeadRedirectNegative(&notPhone)

			a.G.Expect(text).Should(gomega.Equal(wrongPhoneText))
			//a.G.Expect(field).Should(gomega.Equal(fieldPhone))
		}),
	)
	allure.Test(t,
		allure.Description("not transferred country"),
		allure.Action(func() {
			notGeo := *r
			notGeo.LeadParams.CountryCode = ""
			text, field := a.CreateLeadRedirectNegative(&notGeo)

			a.G.Expect(text).Should(gomega.Equal(emptyEnText))
			a.G.Expect(field).Should(gomega.Equal(fieldGeo))
		}),
	)
	allure.Test(t,
		allure.Description("wrong country"),
		allure.Action(func() {
			notGeo := *r
			notGeo.LeadParams.CountryCode = "DER"
			text, _ := a.CreateLeadRedirectNegative(&notGeo)
			log.Printf("---------- text: %s", text)
			a.G.Expect(text).Should(gomega.Equal(notValidGeoText))
			//a.G.Expect(field).Should(gomega.Equal(fieldGeo))
		}),
	)
	allure.Test(t,
		allure.Description("not transferred r_id"),
		allure.Action(func() {
			notRID := *r
			notRID.LeadParams.RID = ""
			text, field := a.CreateLeadRedirectNegative(&notRID)

			a.G.Expect(text).Should(gomega.Equal(emptyRIDText))
			a.G.Expect(field).Should(gomega.Equal(fieldRID))
		}),
	)
	allure.Test(t,
		allure.Description("broken r_id"),
		allure.Action(func() {
			brokenRID := *r
			str := strings.Replace(brokenRID.LeadParams.RID, "-", "", -1)
			brokenRID.LeadParams.RID = str
			text, field := a.CreateLeadRedirectNegative(&brokenRID)

			a.G.Expect(text).Should(gomega.Equal(emptyRIDText))
			a.G.Expect(field).Should(gomega.Equal(fieldRID))
		}),
	)
}

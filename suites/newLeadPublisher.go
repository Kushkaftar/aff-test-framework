package suites

import (
	"aff-test-framework/assertions"
	"aff-test-framework/pkg/config"
	"aff-test-framework/pkg/lead"
	"github.com/dailymotion/allure-go"
	"github.com/onsi/gomega"
	"testing"
)

func TestNewLeadPublisher(t *testing.T, c config.Config) {
	l := lead.NewLead(c)

	allure.Test(t, allure.Action(func() {
		a := assertions.NewAssertion(t)

		allure.Step(
			allure.Description("create lead api publisher"),
			allure.Action(func() {
				a.CreateApiLeadPublisherPositive(l)
			}),
		)
		allure.Step(
			allure.Description("get lead status and expected labels"),
			allure.Action(func() {
				a.GetLeadStatusPositive(l, statusNew)
			}),
		)
	}))

}

func TestNewLeadPublisherNegative(t *testing.T, c config.Config) {
	const (
		brokenHashText = "Lead creation is not allowed"
		wrongHashText  = "Значение не соответствует формату UUID."
		wrongIpText    = "Redirect create exception: Not defined country by ip 10.10.10.10"
		wrongGeoText   = "Goal not found"
		wrongPhoneText = "Некорректно введен номер телефона"
		emptyText      = "Значение не должно быть пустым."
		fieldFlow      = "flow_uuid"
		fieldGeo       = "country_code"
		fieldIP        = "ip"
		fieldPhone     = "phone"
	)

	l := lead.NewLead(c)

	t.Run("not transferred hash", func(t *testing.T) {

		notHash := *l
		notHash.Body.FlowUuid = ""

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			text, field := a.CreateApiLeadPublisherNegative(&notHash)
			a.G.Expect(text).Should(gomega.Equal(emptyText))
			a.G.Expect(field).Should(gomega.Equal(fieldFlow))
		}))

	})
	t.Run("broken hash", func(t *testing.T) {

		notHash := *l
		notHash.Body.FlowUuid = "c6c58e4e-b786-4a70-b65d-639bd6e0b53e"

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			text, _ := a.CreateApiLeadPublisherNegative(&notHash)

			a.G.Expect(text).Should(gomega.Equal(brokenHashText))
		}))

	})
	t.Run("wrong hash", func(t *testing.T) {

		wrongHash := *l
		wrongHash.Body.FlowUuid = "qwerty"

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			text, _ := a.CreateApiLeadPublisherNegative(&wrongHash)

			a.G.Expect(text).Should(gomega.Equal(wrongHashText))
		}))

	})
	t.Run("not transferred IP", func(t *testing.T) {
		notIP := *l
		notIP.Body.Ip = ""

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			text, field := a.CreateApiLeadPublisherNegative(&notIP)

			a.G.Expect(text).Should(gomega.Equal(emptyText))
			a.G.Expect(field).Should(gomega.Equal(fieldIP))
		}))

	})
	t.Run("wrong IP", func(t *testing.T) {
		notIP := *l
		notIP.Body.Ip = "10.10.10.10"

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			text, _ := a.CreateApiLeadPublisherNegative(&notIP)

			a.G.Expect(text).Should(gomega.Equal(wrongIpText))
		}))

	})
	t.Run("not transferred geo", func(t *testing.T) {
		notGeo := *l
		notGeo.Body.CountryCode = ""

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			text, field := a.CreateApiLeadPublisherNegative(&notGeo)

			a.G.Expect(text).Should(gomega.Equal(emptyText))
			a.G.Expect(field).Should(gomega.Equal(fieldGeo))
		}))

	})
	t.Run("wrong geo", func(t *testing.T) {
		wrongGeo := *l
		wrongGeo.Body.CountryCode = "US"

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			text, _ := a.CreateApiLeadPublisherNegative(&wrongGeo)

			a.G.Expect(text).Should(gomega.Equal(wrongGeoText))
		}))

	})
	t.Run("not transferred phone", func(t *testing.T) {
		notPhone := *l
		notPhone.Body.Phone = ""

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			text, field := a.CreateApiLeadPublisherNegative(&notPhone)

			a.G.Expect(text).Should(gomega.Equal(emptyText))
			a.G.Expect(field).Should(gomega.Equal(fieldPhone))
		}))

	})
	t.Run("wrong phone", func(t *testing.T) {
		wrongPhone := *l
		wrongPhone.Body.Phone = "+579100000000"

		allure.Test(t, allure.Action(func() {
			a := assertions.NewAssertion(t)

			text, _ := a.CreateApiLeadPublisherNegative(&wrongPhone)
			a.G.Expect(text).Should(gomega.Equal(wrongPhoneText))
		}))

	})
}
